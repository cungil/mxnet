(in-package :mxnet.predict.ffi)

;; to regenerate the autowrap definitions, delete or rename the .spec files
;; requires LLVM/Clang and c2ffi https://github.com/rpav/c2ffi

;; ffi/c_predict_api.h and ffi/c_api.h come from include/mxnet in the mxnet sources

(autowrap:c-include '(mxnet-predict ffi "c_predict_api.h") :spec-path '(mxnet-predict ffi))

(cl:let ((library (cl:merge-pathnames "mxnet/lib/libmxnet.so" (cl:user-homedir-pathname))))
  #+(and linux sbcl) (sb-int:with-float-traps-masked (:invalid :overflow :divide-by-zero)
		       (cffi:load-foreign-library library))
  #-(and linux sbcl) (cffi:load-foreign-library library)
  (cl:format cl:t "~%~%   Foreign library ~A loaded~%~%~%" library))




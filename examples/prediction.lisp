(in-package :cl-user)

(ql:quickload :mxnet-predict)

(ql:quickload :opticl)

(ql:quickload :drakma)

;; if the data files do not exist in /tmp/ they will be retrieved automatically (but not stored)
;; use the following shell commands to download the files:
#|
cd /tmp/
wget http://data.dmlc.ml/models/imagenet/squeezenet/squeezenet_v1.1-0000.params
wget http://data.dmlc.ml/mxnet/models/imagenet/squeezenet/squeezenet_v1.1-symbol.json
wget http://data.dmlc.ml/mxnet/models/imagenet/synset.txt
|#

(defun file-as-string (path)
  (with-open-file (s path)
    (let ((str (make-string (file-length s))))
      (read-sequence str s)
      str)))

(defun file-as-bytes (path)
  (with-open-file (s path :element-type 'unsigned-byte)
    (let ((buf (make-array (file-length s) :element-type '(unsigned-byte 8))))
      (read-sequence buf s)
      buf)))

(defvar *labels*
  (or (ignore-errors (with-open-file (in "/tmp/synset.txt")
		       (loop for line = (read-line in nil nil) while line collect line)))
      (warn "file synset.txt not found, downloading...")
      (let ((list (drakma:http-request "http://data.dmlc.ml/models/imagenet/synset.txt")))
	(loop for i = 0 then (1+ j)
	   as j = (position #\Newline list :start i)
	   while j collect (subseq list i j)))))

(defvar *json*
  (or (ignore-errors (file-as-string "/tmp/squeezenet_v1.1-symbol.json"))
      (warn "file .json not found, downloading...")
      (flexi-streams:octets-to-string
       (drakma:http-request "http://data.dmlc.ml/models/imagenet/squeezenet/squeezenet_v1.1-symbol.json"))))

(defvar *params*
  (or (ignore-errors (file-as-bytes "/tmp/squeezenet_v1.1-0000.params"))
      (warn "file .params not found, downloading...")
      (drakma:http-request "http://data.dmlc.ml/models/imagenet/squeezenet/squeezenet_v1.1-0000.params")))

(defvar *predictor*
  (mxpred:predictor *json* *params* '(("data" 1 3 224 224)) :dev-type :cpu))

(defun identify-image (png &optional (top 5) (size 224)) 
  (let* ((image (opticl:coerce-image
		 (opticl:resize-image (if (or (stringp png) (pathnamep png))
					  (opticl:read-png-file png)
					  (flexi-streams:with-input-from-sequence (stream png)
					    (opticl:read-png-stream stream)))
				      size size)
		 'opticl:rgb-image))
	 (data (make-array (* 1 3 size size) :element-type 'single-float
			   :initial-contents (loop for c from 0 below 3
						append (loop for i from c by 3 below (array-total-size image)
							  collect (- (row-major-aref image i) 128.0))))))
    (mxpred:forward *predictor* `(("data" ,data)))
    (subseq (sort (loop for prob in (mxpred:get-output *predictor*)
		     for label in *labels*
		     collect (cons prob label))
		  #'> :key #'car)
	    0 top)))

(defvar *test-images* '("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Blue_cup_of_coffee.svg/500px-Blue_cup_of_coffee.svg.png"
			"https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Sport_car_rim.svg/500px-Sport_car_rim.svg.png"
			"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Perseus1Hfx.png/480px-Perseus1Hfx.png"
			"https://upload.wikimedia.org/wikipedia/commons/7/77/Avatar_cat.png"))

(loop for url in *test-images*
   do (format t "~%~A~%~{~A~%~}" url (identify-image (drakma:http-request url))))

```
% sbcl --load examples/prediction.lisp

WARNING: file synset.txt not found, downloading...
WARNING: file .json not found, downloading...
WARNING: file .params not found, downloading...
[17:05:00] src/nnvm/legacy_json_util.cc:209: Loading symbol saved by previous version v0.8.0. Attempting to upgrade...
[17:05:00] src/nnvm/legacy_json_util.cc:217: Symbol successfully upgraded!

https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Blue_cup_of_coffee.svg/500px-Blue_cup_of_coffee.svg.png
(0.36638525 . n07930864 cup)
(0.2466903 . n03063599 coffee mug)
(0.076206975 . n04398044 teapot)
(0.072429284 . n03532672 hook, claw)
(0.062123984 . n02948072 candle, taper, wax light)

https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Sport_car_rim.svg/500px-Sport_car_rim.svg.png
(0.34169966 . n04254680 soccer ball)
(0.26591402 . n03271574 electric fan, blower)
(0.2330616 . n02974003 car wheel)
(0.021093255 . n04192698 shield, buckler)
(0.020947717 . n03944341 pinwheel)

https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Perseus1Hfx.png/480px-Perseus1Hfx.png
(0.9593329 . n03216828 dock, dockage, docking facility)
(0.017900951 . n04483307 trimaran)
(0.0058579957 . n02981792 catamaran)
(0.005326913 . n04147183 schooner)
(0.0029011793 . n04347754 submarine, pigboat, sub, U-boat)

https://upload.wikimedia.org/wikipedia/commons/7/77/Avatar_cat.png
(0.93543077 . n02123045 tabby, tabby cat)
(0.035738923 . n02123159 tiger cat)
(0.025679206 . n02124075 Egyptian cat)
(0.0017329473 . n02123597 Siamese cat, Siamese)
(7.2914973E-4 . n02127052 lynx, catamount)

```
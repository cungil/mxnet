(in-package :mxnet.predict)

(defun check-errors (x)
  (unless (zerop x)
    (error "~A" (mxpred.ffi:mx-get-last-error))))

(defvar *predictors*
  (tg:make-weak-hash-table :weakness :value))

(defclass predictor ()
  ((pointer :initarg :pointer :accessor pointer)))

(defun free-predictor (handle)
  (if (cffi:null-pointer-p handle)
      (warn "tried to free a null pointer")
      (check-errors (mxpred.ffi:mx-pred-free handle))))

(defun predictor (json params inputs &key (dev-type :cpu) (dev-id 0))
  "each input is a list of the form (name batch dim1 dim2 ....)"
  (assert (member dev-type '(:cpu :gpu)))
  (let ((dev-type (if (symbolp dev-type) (ecase dev-type (:cpu 1) (:gpu 2)) dev-type))
	(num-input-nodes (length inputs))
	(input-names (mapcar #'first inputs))
	(input-dims (mapcar #'rest inputs)))
    (cffi:with-foreign-objects ((handle :pointer)
				(params-array :unsigned-char (length params))
				(keys :pointer num-input-nodes)
				(shape-indptr :unsigned-int (1+ num-input-nodes))
				(shape-data :unsigned-int (loop for dims in input-dims sum (length dims))))
      (loop for i from 0 for name in input-names
	 do (setf (cffi:mem-aref keys :pointer i) (cffi:foreign-string-alloc name)))
      (loop for i below (length params)
	 do (setf (cffi:mem-aref params-array :unsigned-char i) (aref params i)))
      (loop for i from 0
	 for idx in (cons 0 (loop for dims in input-dims with sum = 0 collect (incf sum (length dims))))
	 do (setf (cffi:mem-aref shape-indptr :unsigned-int i) idx))
      (loop for i from 0
	 for dim in (apply #'append input-dims)
	 do (setf (cffi:mem-aref shape-data :unsigned-int i) dim))
      (check-errors (mxpred.ffi:mx-pred-create json params-array (length params) dev-type dev-id
					       num-input-nodes keys shape-indptr shape-data handle))
      (loop for i below num-input-nodes
	 do (cffi:foreign-string-free (cffi:mem-aref keys :pointer i)))
      (let ((predictor (make-instance 'predictor :pointer (cffi:mem-ref handle :pointer))))
	(tg:finalize predictor (lambda () (free-predictor handle)))
	(setf (gethash (cffi:pointer-address handle) *predictors*) predictor)))))

(defmethod forward ((predictor predictor) data)
  (loop for (k v) in data
     do (cffi:with-foreign-object (data-array :float (length v))
	  (loop for i from 0 below (length v)
	     do (setf (cffi:mem-aref data-array :float i) (aref v i)))
	  (check-errors (mxpred.ffi:mx-pred-set-input (pointer predictor) k data-array (length v)))))
  (check-errors (mxpred.ffi:mx-pred-forward (pointer predictor))))

(defmethod get-output-shape ((predictor predictor) index)
  (cffi:with-foreign-objects ((shape-data :pointer) (shape-ndim :unsigned-int))
    (check-errors (mxpred.ffi:mx-pred-get-output-shape (pointer predictor) index shape-data shape-ndim))
    (loop for i below (cffi:mem-ref shape-ndim :unsigned-int)
       collect (cffi:mem-aref (cffi:mem-ref shape-data :pointer) :unsigned-int i))))

(defmethod get-output ((predictor predictor) &optional (index 0))
  (let* ((dims (get-output-shape predictor index))
	 (size (reduce #'* dims)))
    (cffi:with-foreign-object (output-data :float size)
      (check-errors (mxpred.ffi:mx-pred-get-output (pointer predictor) index output-data size))
      (loop for i from 0 below size collect (cffi:mem-aref output-data :float i)))))

;; not implemented : MXPredCreatePartialOut MXPredReshape MXPredPartialForward MXNDListCreate MXNDListGet MXNDListFree 

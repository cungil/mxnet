(defpackage #:mxnet.predict
  (:use #:cl)
  (:nicknames #:mxpred)
  (:export "PREDICTOR" "FORWARD" "GET-OUTPUT"))

(defpackage #:mxnet.predict.ffi
  (:nicknames #:mxpred.ffi))

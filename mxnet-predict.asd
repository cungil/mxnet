(in-package #:asdf)

(defsystem #:mxnet-predict
    :name "mxnet-predict"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "Apache License, Version 2.0"
    :description "Common Lisp interface to https://github.com/dmlc/mxnet"
    :depends-on (:cffi :cl-autowrap :trivial-garbage)
    :serial t
    :components ((:module #:ffi :pathname "ffi" :components ((:static-file "c_predict_api.h")))
		 (:file "package-predict")
		 (:file "wrapper-predict")
		 (:file "predict")))
